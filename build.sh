#!/bin/sh -eu
# build.sh -- a bootstrapped image in a migrated Devuan container
# Copyright (C) 2017  Olaf Meeuwissen
#
# License: GPL-3.0+

MIGRATED_IMAGE=$CI_REGISTRY_IMAGE/migrated:debian-$DEBIAN_VERSION
SUITE=$1
ROOTFS=rootfs

rm -rf $ROOTFS || true

docker pull $MIGRATED_IMAGE
docker run --rm \
       --cap-add SYS_ADMIN \
       --volume $PWD:/devuan \
       --workdir /devuan \
       $MIGRATED_IMAGE ./bootstrap.sh $SUITE $ROOTFS

IMAGE=$CI_REGISTRY_IMAGE
STAMP=$(date +%F)

cat > Dockerfile <<EOF
FROM scratch
ADD $ROOTFS-$SUITE.tar.gz /
CMD ["bash"]
EOF

docker build -t $IMAGE:$SUITE-$STAMP .

docker push $IMAGE:$SUITE-$STAMP
docker tag  $IMAGE:$SUITE-$STAMP $IMAGE:$SUITE
docker push $IMAGE:$SUITE
if test $SUITE = $DEVUAN_RELEASE; then
    docker tag  $IMAGE:$SUITE $IMAGE
    docker push $IMAGE
fi

docker run --rm \
       --cap-add SYS_ADMIN \
       --volume $PWD:/devuan \
       --workdir /devuan \
       $IMAGE ./trim.sh $SUITE $ROOTFS

IMAGE=$CI_REGISTRY_IMAGE/slim
STAMP=$(date +%F)

cat > Dockerfile <<EOF
FROM scratch
ADD $ROOTFS-$SUITE-slim.tar.gz /
CMD ["bash"]
EOF

docker build -t $IMAGE:$SUITE-$STAMP .

docker push $IMAGE:$SUITE-$STAMP
docker tag  $IMAGE:$SUITE-$STAMP $IMAGE:$SUITE
docker push $IMAGE:$SUITE
if test $SUITE = $DEVUAN_RELEASE; then
    docker tag  $IMAGE:$SUITE $IMAGE
    docker push $IMAGE
fi
